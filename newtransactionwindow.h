#ifndef NEWTRANSACTIONWINDOW_H
#define NEWTRANSACTIONWINDOW_H

#include <QWidget>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include "transactionaccessor.h"

namespace Ui {
class NewTransactionWindow;
}

class NewTransactionWindow : public QWidget
{
    Q_OBJECT

public:
    explicit NewTransactionWindow(QWidget *parent = 0);
    ~NewTransactionWindow();

private slots:
    void on_btnAdd_clicked();

    void on_btnCancel_clicked();

private:
    Ui::NewTransactionWindow *ui;
    TransactionAccessor* databaseAccess;
};

#endif // NEWTRANSACTIONWINDOW_H
