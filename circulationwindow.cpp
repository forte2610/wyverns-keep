#include "circulationwindow.h"
#include "ui_circulationwindow.h"

CirculationWindow::CirculationWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CirculationWindow)
{
    ui->setupUi(this);
    ui->statusToolbar->hide();
    databaseAccess = new TransactionAccessor;
    displayModel = new QSqlQueryModel;
    ui->btnStatus->setEnabled(false);
    ui->btnRemove->setEnabled(false);
    ui->btnViewSelected->hide();
    on_pushButton_7_clicked();
}

CirculationWindow::~CirculationWindow()
{
    delete ui;
}

void CirculationWindow::on_btnRefresh_clicked()
{
    ui->tableView->setModel(databaseAccess->getCurrentModel());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void CirculationWindow::recordSelected(const QModelIndex& index) {
    QString ID = index.sibling(index.row(), 0).data().toString();
    selectedTransaction = databaseAccess->getTransaction(ID);
    ui->btnStatus->setEnabled(true);
    ui->btnRemove->setEnabled(true);
}

void CirculationWindow::on_btnStatus_clicked()
{
    ui->statusToolbar->show();
}

void CirculationWindow::on_btnIntact_clicked()
{
    selectedTransaction->setStatus(TSTT_INTACT);
    ui->statusToolbar->hide();
}

void CirculationWindow::on_btnDamaged_clicked()
{
    selectedTransaction->setStatus(TSTT_DAMAGED);
    ui->statusToolbar->hide();
}

void CirculationWindow::on_btnLost_clicked()
{
    selectedTransaction->setStatus(TSTT_LOST);
    ui->statusToolbar->hide();
}

void CirculationWindow::on_pushButton_7_clicked()
{
    ui->tableView->setModel(databaseAccess->getCirculation());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void CirculationWindow::on_btnAdd_clicked()
{
    NewTransactionWindow* NewTransactionChild = new NewTransactionWindow();
    NewTransactionChild->show();
}
