#ifndef NEWREADERWINDOW_H
#define NEWREADERWINDOW_H

#include <QWidget>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include "readeraccessor.h"

namespace Ui {
class NewReaderWindow;
}

class NewReaderWindow : public QWidget
{
    Q_OBJECT

public:
    explicit NewReaderWindow(QWidget *parent = 0);
    ~NewReaderWindow();

private slots:
    void on_btnCancel_clicked();

    void on_btnAdd_clicked();

    void on_btnAddMug_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::NewReaderWindow *ui;
    ReaderAccessor* databaseAccess;
};

#endif // NEWREADERWINDOW_H
