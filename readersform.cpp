#include "readersform.h"
#include "ui_readersform.h"

ReadersForm::ReadersForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReadersForm)
{
    ui->setupUi(this);
    databaseAccess = new ReaderAccessor;
    displayModel = new QSqlQueryModel;
    ui->tableView->setModel(databaseAccess->getReaderList());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
    ui->btnRemove->setEnabled(false);
    ui->btnModify->setEnabled(false);
}

ReadersForm::~ReadersForm()
{
    delete ui;
}

void ReadersForm::on_btnAdd_clicked()
{
    NewReaderWindow* NewReaderChild = new NewReaderWindow();
    NewReaderChild->show();
}

void ReadersForm::refreshContent() {
    ui->tableView->setModel(databaseAccess->getCurrentModel());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void ReadersForm::recordSelected(const QModelIndex &index) {
    // Get selected reader
    QString ID = index.sibling(index.row(), 0).data().toString();
    selectedReader = databaseAccess->getReader(ID);
    // Display information of selected reader
    displayDetailedInfo();
    displayProfilePicture();
    // Enable selected buttons
    ui->btnModify->setEnabled(true);
    ui->btnRemove->setEnabled(true);
}

void ReadersForm::displayDetailedInfo() {
    ui->lblID->setText(selectedReader->getID());
    ui->lblName->setText(selectedReader->getFullName());
    ui->lblGender->setText(selectedReader->getGender());
    ui->lblDOB->setText(selectedReader->getDOB().toString("yy-MM-dd"));
    ui->lblAddress->setText(selectedReader->getAddress());
    ui->lblPhone->setText(selectedReader->getPhone());
    ui->lblEmail->setText(selectedReader->getEmail());
}

void ReadersForm::displayProfilePicture() {
    QImage ProfilePicture(selectedReader->getProfilePicture());
    QGraphicsScene* scene = new QGraphicsScene();
    QPixmap* profilePixmap = new QPixmap(QPixmap::fromImage(ProfilePicture));
    scene->addPixmap(*profilePixmap);
    ui->pctMug->setScene(scene);
    ui->pctMug->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
    ui->pctMug->show();
}

void ReadersForm::on_btnModify_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    QModelIndex index = indexes.at(0);
    QString currentID = index.sibling(index.row(), 0).data().toString();
    EditReaderWindow* EditReaderChild = new EditReaderWindow(0, currentID);
    EditReaderChild->show();
}

void ReadersForm::on_tableView_clicked(const QModelIndex &index)
{
    recordSelected(index);
}

void ReadersForm::on_btnRefresh_clicked()
{
    refreshContent();
}

void ReadersForm::on_btnRemove_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Warning!", "This action is not reversible.\n"
                                                    "Are you sure you want to delete\n"
                                                    "the selected item(s)?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        QModelIndexList selection = ui->tableView->selectionModel()->selectedRows();
        for(int i=0; i< selection.count(); i++)
        {
            QModelIndex index = selection.at(i);
            QString ID = index.sibling(index.row(), 0).data().toString();
            Reader* ReaderToDelete = databaseAccess->getReader(ID);
            databaseAccess->deleteReader(ReaderToDelete);
        }
        ui->btnRemove->setEnabled(false);
    }
    else {}
    refreshContent();
}

void ReadersForm::on_btnSearch_clicked()
{
    QString criterium;
    if(ui->comboBox->currentText() == "ID") criterium = RDSRCH_ID;
    if(ui->comboBox->currentText() == "Name") criterium = RDSRCH_FULLNAME;
    if(ui->comboBox->currentText() == "Date of birth") criterium = RDSRCH_DOB;
    if(ui->comboBox->currentText() == "Gender") criterium = RDSRCH_GENDER;
    if(ui->comboBox->currentText() == "Address") criterium = RDSRCH_ADDRESS;
    if(ui->comboBox->currentText() == "Phone number") criterium = RDSRCH_PHONE;
    if(ui->comboBox->currentText() == "Email") criterium = RDSRCH_EMAIL;
    QString match = ui->txtSearch->text();
    databaseAccess->search(criterium, match);
}
