#ifndef READER_H
#define READER_H

#include <QWidget>
#include <QDate>

class Reader
{
private:
    QString ID;
    QString fullName;
    QDate DOB;
    QString gender;
    QString address;
    QString phone;
    QString email;
    QString profilepicture;
public:
    Reader();
    QString getID();
    QString getFullName();
    QDate getDOB();
    QString getGender();
    QString getAddress();
    QString getPhone();
    QString getEmail();
    QString getProfilePicture();
    void setID(QString new_ID);
    void setName(QString new_name);
    void setDOB(QDate new_DOB);
    void setGender(QString new_gender);
    void setAddress(QString new_address);
    void setPhone(QString new_phone);
    void setEmail(QString new_email);
    void setProfilePicture(QString new_pic);
};

#endif // READER_H
