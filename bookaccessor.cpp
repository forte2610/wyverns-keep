#include "bookaccessor.h"

BookAccessor::BookAccessor()
{
    CTL = QSqlDatabase::database();
    dataModel = new QSqlQueryModel;
    getCatalogue();
}

QSqlQueryModel* BookAccessor::getCurrentModel() {
    dataModel->setQuery(currentQuery);
    return dataModel;
}

QSqlQueryModel* BookAccessor::getCatalogue() {
    currentQuery = "SELECT bookID AS 'Book ID', ISBN AS 'ISBN', "
                        "title AS 'Title', booktype AS 'Type', genre AS 'Genre', "
                        "author AS 'Author', publisher AS 'Publisher', "
                        "yearpublished AS 'Year published', length AS "
                        "'Number of pages', quantity AS 'In stock', "
                        "summary AS 'Summary' FROM BOOKS", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

Book* BookAccessor::getBook(QString ID) {
    QSqlQuery getBookQuery;
    getBookQuery.exec("SELECT * FROM BOOKS WHERE "
                    "bookID = '" + ID +"'");
    getBookQuery.first();
    Book* returnValue = new Book;
    returnValue->setID(getBookQuery.value(0).toString());
    returnValue->setISBN(getBookQuery.value(1).toString());
    returnValue->setTitle(getBookQuery.value(2).toString());
    returnValue->setType(getBookQuery.value(3).toString());
    returnValue->setGenre(getBookQuery.value(4).toString());
    returnValue->setAuthor(getBookQuery.value(5).toString());
    returnValue->setPublisher(getBookQuery.value(6).toString());
    returnValue->setYearPublished(getBookQuery.value(7).toInt());
    returnValue->setLength(getBookQuery.value(8).toInt());
    returnValue->setQuantity(getBookQuery.value(9).toInt());
    returnValue->setSummary(getBookQuery.value(10).toString());
    returnValue->setCoverArtURL(getBookQuery.value(11).toString());
    return returnValue;
}

void BookAccessor::addBook(Book* newBook) {
    QSqlQuery addBookQuery;
    addBookQuery.exec("INSERT INTO BOOKS (bookID, ISBN, title, "
                      "booktype, genre, author, publisher, yearpublished, "
                      "length, quantity, summary, coverart) VALUES ('"
                      + newBook->getID() + "', '" + newBook->getISBN()
                      + "', '" + newBook->getTitle()
                      + "', '" + newBook->getType() + "', '" + newBook->getGenre()
                      + "', '" + newBook->getAuthor() + "', '"
                      + newBook->getPublisher() + "', "
                      + QString::number(newBook->getYearPublished())
                      + ", " + QString::number(newBook->getLength())
                      + ", " + QString::number(newBook->getQuantity())
                      + ", '" + newBook->getSummary() + "', '"
                      + newBook->getCoverArtURL()+ "')");
}

QSqlQueryModel* BookAccessor::search(QString criterium, QString match) {
    if ((criterium == "yearpublished") || (criterium == "quantity") ||
            (criterium == "length")) {
        currentQuery = "SELECT bookID AS 'Book ID', ISBN AS 'ISBN', "
                            "title AS 'Title', booktype AS 'Type', genre AS 'Genre', "
                            "author AS 'Author', publisher AS 'Publisher', "
                            "yearpublished AS 'Year published', length AS "
                            "'Number of pages', quantity AS 'In stock', "
                            "summary AS 'Summary' FROM BOOKS "
                            "WHERE " + criterium + " = " + match, CTL;
        dataModel->setQuery(currentQuery);
        return dataModel;
    }
    else {
        currentQuery = "SELECT bookID AS 'Book ID', ISBN AS 'ISBN', "
                            "title AS 'Title', booktype AS 'Type', genre AS 'Genre', "
                            "author AS 'Author', publisher AS 'Publisher', "
                            "yearpublished AS 'Year published', length AS "
                            "'Number of pages', quantity AS 'In stock', "
                            "summary AS 'Summary' FROM BOOKS "
                            "WHERE " + criterium + " LIKE '%" + match + "%'", CTL;
        dataModel->setQuery(currentQuery);
        return dataModel;
    }
}

QSqlQueryModel* BookAccessor::getFilterBooks() {
    currentQuery = "SELECT bookID AS 'Book ID', ISBN AS 'ISBN', "
                        "title AS 'Title', booktype AS 'Type', genre AS 'Genre', "
                        "author AS 'Author', publisher AS 'Publisher', "
                        "yearpublished AS 'Year published', length AS "
                        "'Number of pages', quantity AS 'In stock', "
                        "summary AS 'Summary' FROM BOOKS WHERE "
                        "booktype = 'Book'", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

QSqlQueryModel* BookAccessor::getFilterMagazines() {
    currentQuery = "SELECT bookID AS 'Book ID', ISBN AS 'ISBN', "
                        "title AS 'Title', booktype AS 'Type', genre AS 'Genre', "
                        "author AS 'Author', publisher AS 'Publisher', "
                        "yearpublished AS 'Year published', length AS "
                        "'Number of pages', quantity AS 'In stock', "
                        "summary AS 'Summary' FROM BOOKS WHERE "
                        "booktype = 'Magazine'", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

QSqlQueryModel* BookAccessor::getFilterNewspapers() {
    currentQuery = "SELECT bookID AS 'Book ID', ISBN AS 'ISBN', "
                        "title AS 'Title', booktype AS 'Type', genre AS 'Genre', "
                        "author AS 'Author', publisher AS 'Publisher', "
                        "yearpublished AS 'Year published', length AS "
                        "'Number of pages', quantity AS 'In stock', "
                        "summary AS 'Summary' FROM BOOKS WHERE "
                        "booktype = 'Newspaper'", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

void BookAccessor::deleteBook(Book* BookToDelete) {
    QSqlQuery addBookQuery;
    addBookQuery.exec("DELETE FROM BOOKS WHERE "
                      "bookID = '" + BookToDelete->getID()
                      + "'");
}

void BookAccessor::updateBook(Book* BookToUpdate) {
    QSqlQuery updateBookQuery;
    updateBookQuery.exec("UPDATE BOOKS SET bookID = '"
                         + BookToUpdate->getID() + "', ISBN = '" + BookToUpdate->getISBN()
                         + "', title = '" + BookToUpdate->getTitle()
                         + "', booktype = '" + BookToUpdate->getType()
                         + "', genre = '" + BookToUpdate->getGenre()
                         + "', author = '" + BookToUpdate->getAuthor()
                         + "', publisher = '" + BookToUpdate->getPublisher()
                         + "', yearpublished = " + QString::number(BookToUpdate->getYearPublished())
                         + ", length = " + QString::number(BookToUpdate->getLength())
                         + ", quantity = " + QString::number(BookToUpdate->getQuantity())
                         + ", summary = '" + BookToUpdate->getSummary()
                         + "', coverart = '" + BookToUpdate->getCoverArtURL()+ "' "
                         + " WHERE bookID = '" + BookToUpdate->getID() + "'");
}
