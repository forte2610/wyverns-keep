#ifndef TRANSACTIONACCESSOR_H
#define TRANSACTIONACCESSOR_H

#include "transaction.h"
#include "bookaccessor.h"
#include "readeraccessor.h"
#include <QSql>
#include <QDebug>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>

#define TSTT_INTACT "Intact"
#define TSTT_DAMAGED "Damaged"
#define TSTT_LOST "Lost"

class TransactionAccessor
{
private:
    QSqlDatabase CTL;
    QSqlQueryModel* dataModel;
    QString currentQuery;
public:
    TransactionAccessor();
    QSqlQueryModel* getCurrentModel();
    QSqlQueryModel* getCirculation();
    QSqlQueryModel* search(QString criterium, QString match);
    Transaction* getTransaction(QString ID);
    void addTransaction(Transaction* newTransaction);
    void deleteTransaction(Transaction* TransactionToDelete);
    void updateTransaction(Transaction* TransactionToUpdate);
};

#endif // TRANSACTIONACCESSOR_H
