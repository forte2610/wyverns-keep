#include "newreaderwindow.h"
#include "ui_newreaderwindow.h"

NewReaderWindow::NewReaderWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewReaderWindow)
{
    ui->setupUi(this);
    QDate cdate = QDate::currentDate();
    ui->deDOB->setDate(cdate);
}

NewReaderWindow::~NewReaderWindow()
{
    delete ui;
}

void NewReaderWindow::on_btnCancel_clicked()
{
    this->close();
}

void NewReaderWindow::on_btnAdd_clicked()
{
    Reader* toBeAdded = new Reader;
    toBeAdded->setID(ui->txtID->text());
    toBeAdded->setName(ui->txtName->text());
    toBeAdded->setDOB(ui->deDOB->date());
    toBeAdded->setGender(ui->cmbGender->currentText());
    toBeAdded->setAddress(ui->txtAddress->text());
    toBeAdded->setPhone(ui->txtPhone->text());
    toBeAdded->setEmail(ui->txtEmail->text());
    toBeAdded->setProfilePicture(ui->txtCoverURL->toPlainText());
    databaseAccess->addReader(toBeAdded);
    this->close();
}

void NewReaderWindow::on_btnAddMug_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Image File",
    QDir::homePath());
       if(!fileName.isEmpty())
       {
           QImage image(fileName);
           if(image.isNull())
           {
               ui->txtCoverURL->setPlainText("Failed to display image");
           }
           QGraphicsScene* scene = new QGraphicsScene();
           QPixmap* coverPreview = new QPixmap(QPixmap::fromImage(image));
           scene->addPixmap(*coverPreview);
           ui->pctCover->setScene(scene);
           ui->pctCover->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
           ui->txtCoverURL->setPlainText(fileName);
       }
}

void NewReaderWindow::on_pushButton_2_clicked()
{
    ui->txtCoverURL->clear();
    ui->pctCover->scene()->clear();
    ui->pctCover->update();
}
