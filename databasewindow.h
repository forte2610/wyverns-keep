#ifndef DATABASEWINDOW_H
#define DATABASEWINDOW_H

#include <QWidget>
#include <QtSql/QSqlDatabase>

#define CMB_MYSQL 0
#define CMB_SQLSERVER 1

namespace Ui {
class DatabaseWindow;
}

class DatabaseWindow : public QWidget
{
    Q_OBJECT

public:
    explicit DatabaseWindow(QWidget *parent = 0);
    ~DatabaseWindow();

signals:
    void connectDatabase(bool ok);

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::DatabaseWindow *ui;
    QSqlDatabase db;
};

#endif // DATABASEWINDOW_H
